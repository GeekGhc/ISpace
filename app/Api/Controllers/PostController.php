<?php
namespace App\Api\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Discussion;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    //获取帖子列表
    public function getList(Request $request)
    {
        $limitCount = $request->get('limit')?$request->get("limit"):6;
        $page = $request->get('page')?$request->get('page'):0;
        $type = $request->get('type')?$request->get('type'):'';
        $posts = Discussion::with('user')->skip($limitCount*$page)->orderBy('created_at','desc')->take($limitCount)->get();
        $colection = $posts->map(function($item,$key){
            $item->createdAt = $item->created_at->diffForHumans();
            $item->commentCount = $item->comment_count;
            $item->author = $item->user->name;
        });
        return response()->json([
            'code'=>1,
            'data'=>$posts,
            'message'=>'success'
        ]);
    }

    //帖子详情
    public function getDetail($id)
    {
        $post = Discussion::with('user')->find($id);
        return response()->json([
            'code'=>1,
            'message'=>'查询成功',
            'data'=>$post
        ]);
    }

    public function createPost(Request $request)
    {

    }

    public function getData()
    {
        return "{\"bgImage\":\"http://ov7yoiydx.bkt.clouddn.com/bg_image.jpg\",\"gameArea\":\"苹果手Q39区\",\"gameName\":\"VivanFLY\",\"gameSegment\":\"最强王者\",\"replyItemList\":[{\"commentItemList\":[{\"content\":\"胖胖，国服luna\",\"female\":true,\"userName\":\"傻乎乎的等着\"}],\"commentNumber\":141,\"from\":\"助手好友\",\"gameArea\":\"苹果手Q7区1\",\"gameName\":\"零度ESON1111\",\"gameSegment\":\"荣耀黄金Ⅲ\",\"male\":true,\"praiseNumber\":323,\"replyContent\":\"好像不能发自己qq号，送三张狮子壁纸给大家\",\"replyImage\":[{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"},{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"},{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"}],\"time\":\"7小时前\",\"userImage\":\"http://ov7yoiydx.bkt.clouddn.com/male_one.jpg\",\"userList\":[\"自由飞翔\",\"送开心\",\"张三\",\"李四\"],\"userName\":\"Winter零度1\",\"vip\":true},{\"commentItemList\":[{\"content\":\"胖胖，国服luna\",\"female\":true,\"userName\":\"傻乎乎的等着\"}],\"commentNumber\":141,\"from\":\"助手好友\",\"gameArea\":\"苹果手Q7区\",\"gameName\":\"零度ESON\",\"gameSegment\":\"荣耀黄金Ⅲ\",\"male\":true,\"praiseNumber\":363,\"replyContent\":\"好像不能发自己qq号，送三张狮子壁纸给大家\",\"replyImage\":[{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"},{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"},{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"}],\"time\":\"7小时前\",\"userImage\":\"http://ov7yoiydx.bkt.clouddn.com/male_one.jpg\",\"userList\":[\"自由飞翔\",\"送开心\",\"张三\",\"李四\"],\"userName\":\"Winter零度\",\"vip\":true},{\"commentItemList\":[{\"content\":\"胖胖，国服luna\",\"female\":true,\"userName\":\"傻乎乎的等着\"}],\"commentNumber\":141,\"from\":\"助手好友\",\"gameArea\":\"苹果手Q7区\",\"gameName\":\"零度ESON\",\"gameSegment\":\"荣耀黄金Ⅲ\",\"male\":true,\"praiseNumber\":363,\"replyContent\":\"好像不能发自己qq号，送三张Luna壁纸给大家\",\"replyImage\":[{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"},{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"}],\"time\":\"7小时前\",\"userImage\":\"http://ov7yoiydx.bkt.clouddn.com/male_two.jpg\",\"userList\":[\"自由飞翔\",\"送开心\",\"张三\",\"李四\"],\"userName\":\"Winter零度\",\"vip\":false},{\"commentItemList\":[{\"content\":\"胖胖，国服luna\",\"female\":true,\"userName\":\"傻乎乎的等着\"}],\"commentNumber\":141,\"from\":\"助手好友\",\"gameArea\":\"苹果手Q7区\",\"gameName\":\"零度ESON\",\"gameSegment\":\"荣耀黄金Ⅲ\",\"male\":true,\"praiseNumber\":363,\"replyContent\":\"好像不能发自己qq号，送三张貂蝉壁纸给大家\",\"replyImage\":[{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"}],\"time\":\"7小时前\",\"userImage\":\"http://ov7yoiydx.bkt.clouddn.com/female.jpg\",\"userList\":[\"自由飞翔\",\"送开心\",\"张三\",\"李四\"],\"userName\":\"Winter零度\",\"vip\":true},{\"commentItemList\":[{\"content\":\"胖胖，国服luna\",\"female\":true,\"userName\":\"傻乎乎的等着\"}],\"commentNumber\":141,\"from\":\"助手好友\",\"gameArea\":\"苹果手Q7区\",\"gameName\":\"零度ESON\",\"gameSegment\":\"荣耀黄金Ⅲ\",\"male\":true,\"praiseNumber\":363,\"replyContent\":\"好像不能发自己qq号，送三张狮子壁纸给大家\",\"replyImage\":[{\"contentImage\":\"http://ov7yoiydx.bkt.clouddn.com/lions.jpg\"}],\"time\":\"7小时前\",\"userImage\":\"http://ov7yoiydx.bkt.clouddn.com/female.jpg\",\"userList\":[\"自由飞翔\",\"送开心\",\"张三\",\"李四\"],\"userName\":\"Winter零度\",\"vip\":false}],\"userImage\":\"http://ov7yoiydx.bkt.clouddn.com/male_three.jpg\",\"userName\":\"小裤衩\"}";
    }
}