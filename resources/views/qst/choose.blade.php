<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>队伍关卡选择</title>
    <link href="https://cdn.bootcss.com/semantic-ui/2.2.10/semantic.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/qst/css/style.css">
</head>
<body>
<div class="container">
    <div class="ui three column centered grid">
        <div class="column">
            <h2 class="choose-title">队伍关卡选择</h2>
            <form class="ui form" action="{{url('/qst/choose/station')}}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="two fields">
                    <div class="team field">
                        <label>队伍</label>
                        <select class="ui fluid dropdown" id="team" name="team">
                            <option value="">队伍</option>
                            <option value="第一队">第一队</option>
                            <option value="第二队">第二队</option>
                            <option value="第三队">第三队</option>
                            <option value="第四队">第四队</option>
                            <option value="第五队">第五队</option>
                            <option value="第六队">第六队</option>
                            <option value="第七队">第七队</option>
                        </select>
                    </div>
                    <div class="route field">
                        <label>线路</label>
                        <select class="ui fluid dropdown" id="route" name="route">
                            <option value="">线路</option>
                            <option value="A">线路A</option>
                            <option value="B">线路B</option>
                        </select>
                    </div>
                    <div class="station field">
                        <label>关卡</label>
                        <select class="ui fluid dropdown" id="station" name="station">
                            <option value="">关卡</option>
                            <option value="基地">基地</option>
                            <option value="龙游河公园">龙游河公园</option>
                            <option value="大润发">大润发</option>
                            <option value="安定广场">安定广场</option>
                            <option value="水绘园">水绘园</option>
                            <option value="红十四军">红十四军</option>
                            <option value="奥体中心">奥体中心</option>
                        </select>
                    </div>
                </div>
                <button class="ui button facebook" type="submit" id="submit">提交</button>
            </form>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script>
    /*$("#submit").on('click',function () {
        var team = $('#team').val();
        var station = $('#station').val();
        if(team && station){
            $.ajax({
                type:'GET',
                url:'./save.php',
                dataType:'json',
                data:{
                    'team':team,
                    'station':station
                },
                success:function(json){
                    if(json.code){
                        alert("选择成功")
                    }else{
                        alert("写入失败")
                    }
                },
                error:function(){
                    alert("出错了")
                }
            });
        }else {
            alert("请选择具体的关卡和队伍")
        }
    })*/
</script>
</body>
</html>