<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['middleware'=>'api','namespace' => 'App\Api\Controllers'], function () {
    Route::get('user/{userId}','UserController@getUser');
    Route::post('user/register','UserController@register');
    Route::post('user/login','UserController@login');
    Route::patch('user/pwd','UserController@editPwd');

    Route::patch('user/edit/name','ProfileController@setName');
    Route::patch('user/edit/location','ProfileController@setLocation');
    Route::patch('user/edit/desc','ProfileController@setDesc');
    Route::patch('user/edit/site','ProfileController@setSite');
    Route::patch('user/edit/work','ProfileController@setWork');


    Route::get('articles/hot/list','ArticleController@getHotList');

    Route::get('posts/list','PostController@getList');
    Route::get('posts/show/{id}','PostController@getDetail');
    Route::post('posts/create','PostController@createPost');

    Route::post('test/',function (Request $request){
       return response()->json([
           'code'=>1,
           'name'=>$request->get('name')
       ]);
    });

    Route::get('qst/data','PostController@getData');
});
