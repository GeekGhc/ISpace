$(function () {
    $("#search-content").keydown(function (event) {
        if (event.keyCode == 13) {
            var content = $(this).val();
            location.href = "/search/post?q=" + content;
        }
    })

    $('.section-content').css('min-height', "580px");
    $('.notify-section-content').css('min-height', "480px");
    $('.favorite-section-content').css('min-height', "440px");

    $('.vjs-menu-item').on('click', function () {
        $('.vjs-menu-item').removeClass('aria-selected');
        $(this).addClass('aria-selected');
    })


    $(document).scroll(function (event) {
        if ($(document).scrollTop() > 76) {
            $(".navbar-fixed-top").addClass("opaque")
        } else {
            $(".navbar-fixed-top").removeClass("opaque")
        }

        if ($(document).scrollTop() >= 234) {
            $('#backToTop').fadeIn(400)
        } else {
            $('#backToTop').fadeOut(400)
        }

    })
    $('#backToTop').on('click', function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    });

    $(".feature-box").hover(function () {
        $(this).children().children(".info").animate({top: "0"}, "normal")
    }, function () {
        $(this).children().children(".info").animate({top: "100%"}, "normal",)
    })
});
var one = $("#section-one").offset().top;
var two = $("#section-two").offset().top;
var three = $("#section-three").offset().top;
var four = $("#section-four").offset().top;
var five = $("#section-five").offset().top;
var six = $("#section-six").offset().top;

$(document).scroll(function (event) {
    if ($(document).scrollTop() > (two - 500)) {
        $("#section-two").find(".animated").addClass("fadeInUp")
    }
    if ($(document).scrollTop() > (three - 500)) {
        $("#section-three").find(".animated").addClass("fadeInUp")
    }
    if ($(document).scrollTop() > (four - 500)) {
        $("#section-four").find(".animated").addClass("fadeInUp")
    }
    if ($(document).scrollTop() > (parseInt(five)-500)) {
        $("#section-five").find(".animated-up").addClass("fadeInUp")
        $("#section-five").find(".animated-left").addClass("fadeInLeft")
        $("#section-five").find(".animated-right").addClass("fadeInRight")
    }
    if ($(document).scrollTop() > (six - 500)) {
        $("#section-six").find(".animated").addClass("fadeInUp")
    }
})