<?php
namespace App\Api\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //设置用户的简介
    public function setName(Request $request)
    {
        $res = User::where('name',$request->get('name'))->count();
        if($res){
            return response()->json([
                'code'=>0,
                'message'=>'用户名已经存在',
                'data'=>null,
            ]);
        }
        $user = User::find($request->get('userId'));
        $user->name = $request->get('name');
        if($user->save()){
            return response()->json([
                'code'=>1,
                'message'=>"success",
                'data'=>$user->name,
            ]);
        }else{
            return response()->json([
                'code'=>0,
                'message'=>'failed',
                'data'=>null,
            ]);
        }
    }

    //修改用户所在城市
    public function setLocation(Request $request)
    {
        $profile = Profile::where($request->get('userId'))->first();
        $profile->city = $request->get('city');
        if($profile->save()){
            return response()->json([
                'code'=>1,
                'message'=>"success",
                'data'=>$profile->city,
            ]);
        }else{
            return response()->json([
                'code'=>0,
                'message'=>'failed',
                'data'=>null,
            ]);
        }

    }

    //设置用户的简介
    public function setDesc(Request $request)
    {
        $profile = Profile::where($request->get('userId'))->first();
        $profile->description = $request->get('desc');
        if($profile->save()){
            return response()->json([
                'code'=>1,
                'message'=>"success",
                'data'=>$profile->description,
            ]);
        }else{
            return response()->json([
                'code'=>0,
                'message'=>'failed',
                'data'=>null,
            ]);
        }
    }

    //设置用户的域名
    public function setSite(Request $request)
    {
        $profile = Profile::where($request->get('userId'))->first();
        $profile->website = $request->get('site');
        if($profile->save()){
            return response()->json([
                'code'=>1,
                'message'=>"success",
                'data'=>$profile->website,
            ]);
        }else{
            return response()->json([
                'code'=>0,
                'message'=>'failed',
                'data'=>null,
            ]);
        }
    }
}