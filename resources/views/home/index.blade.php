<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta id="token" name="token" value="{{ csrf_token() }}">
    <title>CodeSpace 社区</title>
    <link rel="stylesheet" href="/css/source/bootstrap.css">
    <link rel="stylesheet" href="/css/source/semantic.min.css">
    <link rel="stylesheet" href="/css/source/font-awesome.min.css">
    <link rel="stylesheet" href="{{elixir('css/app.css')}}">
    <link rel="stylesheet" href="/css/animation.css">
    <link rel="stylesheet" href="/css/home.css">
</head>
<body>
<header class="site-header transparent">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <navbar-header>
                <a class="navbar-brand" href="/">CodeSpace</a>
            </navbar-header>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse" style="margin-top: 12px">
                <form class="navbar-form navbar-left">
                    <div class="form-group">
                        <input id="search-content" type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/lessons">视频</a></li>
                    <li><a href="/discussion">问答</a></li>
                    <li><a href="/article">文章</a></li>
                    <li><a href="/donate-to-me">订阅</a></li>
                    <li><a href="#">活动</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">撰写 <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{url('/article/create')}}">写文章</a></li>
                            <li><a href="{{url('/discussion/create')}}">提问题</a></li>
                            <li><a href="#">做笔记</a></li>
                            <li class="divider"></li>
                            <li><a href="#">草稿箱</a></li>
                        </ul>
                    </li>

                    @if(\Auth::check())
                        <li><a href="/user/notifications" class="message-info">
                                <i class="fa fa-bell-o">
                                    @if(Auth::user()->unreadNotifications->count()!==0)
                                        <span class="badge">{{\Auth::user()->unreadNotifications->count()}}</span>
                                    @endif
                                </i>
                            </a>
                        </li>
                        <li>
                            <a id="dLabel" type="button" data-toggle="dropdown" href="#">
                                {{Auth::user()->name}}
                            </a>
                        </li>
                        <li class="dropdown">
                            <a id="dLabel" type="button" data-toggle="dropdown"
                               style="padding: 0px 0px 2px;cursor: pointer">
                                <img src="{{\Auth::User()->avatar}}" class="img-circle" width="44px" height="44px"
                                     style="margin-top: 3px;border: 1px solid #fff;cursor: pointer"
                                     alt="">
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="/u/{{\Auth::user()->name}}"><i class="fa fa-user fa-mr"></i>我的主页</a>
                                </li>
                                <li>
                                    <a href="/user/account"><i class="fa  fa-cog fa-mr"></i>账户设置</a>
                                </li>
                                <li>
                                    <a href="/user/favorites"><i class="fa fa-heart fa-mr"></i>我的收藏</a>
                                </li>
                                <li>
                                    <a href="/broadcasts/music"><i class="fa fa-microphone fa-mr"></i>音乐电台</a>
                                </li>
                                <li>
                                    <a href="/user/password"><i class="fa fa-lock fa-mr"></i>修改密码</a>
                                </li>
                                <li role="separator" class="divider fa-mr"></li>
                                <li>
                                    <a href="/logout"><i class="fa fa-sign-out fa-mr"></i>退出登录</a>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li><a href="/user/login">登录</a></li>
                        <li><a href="/user/register">注册</a></li>
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
</header>
<section id="home_wrapper" class="home-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="home-title animated fadeIn" data-delay="0.5s" data-animation="fadeInUp"
                    style="animation-delay: 0.5s; animation-name:fadeInUp;">CodeSpace Community</h1>
                <p class="home-subtitle animated fadeIn" data-delay="1s"
                   style="animation-delay: 1s; animation-name: fadeInUp;">创造属于开发者的时代</p>
                <a class="btn btn-lg btn-margin btn-white-transparent animated fadeIn" href="#" data-delay="1.5s"
                   style="animation-delay: 1.5s; animation-name: fadeInUp;">Start Here</a>
            </div>
        </div>
    </div>
</section>
<section class="section-one" id="section-one">
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center animated fadeInUp"
                     style="opacity:0;animation-delay: 0.5s;">
                    <h1 class="title">为你提供社区化的你想要的一切</h1>
                    <p class="intro">更丰富的社区活动元素</p>
                </div>
            </div>
            <div class="feature-box col-md-3 animated fadeInUp" style="opacity:0;">
                <div class="inner">
                    <div class="front" style="opacity: 1">
                        <i class="icon-img fa fa-stack-overflow"></i>
                        <h3 class="front-title">技术帖子问答</h3>
                        <span>各类技术性问题讨论解答</span>
                    </div>
                    <div class="info">
                        提供各类技术问题 用户交流学习 如果有疑问的随时发帖提问 当然也可以回答问题贴会有相应的经验
                    </div>
                </div>
            </div>
            <div class="feature-box col-md-3 animated fadeInUp" style="opacity:0;">
                <div class="inner">
                    <div class="front" style="opacity: 1">
                        <i class="icon-img fa fa-book"></i>
                        <h3 class="front-title">文章心得分享</h3>
                        <span>分享平时的所学心得</span>
                    </div>
                    <div class="info">
                        这里分享我平时的心得和文章 记录平时所学 当然也会转载一些非常有用的技术文章和人生感悟
                    </div>
                </div>
            </div>
            <div class="feature-box col-md-3 animated fadeInUp" style="opacity:0;">
                <div class="inner">
                    <div class="front" style="opacity: 1">
                        <i class="icon-img fa fa-coffee"></i>
                        <h3 class="front-title">用户特权优化</h3>
                        <span>用户特权持续更新</span>
                    </div>
                    <div class="info">
                        丰富用户的特权 增强用户的社区参与度 给开发者一个相对丰富的选择
                    </div>
                </div>
            </div>
            <div class="feature-box col-md-3 animated fadeInUp">
                <div class="inner">
                    <div class="front" style="opacity: 1">
                        <i class="icon-img fa fa-desktop"></i>
                        <h3 class="front-title">多平台支持</h3>
                        <span>支持Web端和安卓端 随时查看讯息</span>
                    </div>
                    <div class="info">
                        该社区不仅支持Web端还会支持移动端 这样工作业余时间都可以了解自己想要的想学的
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-two" id="section-two">
    <div class="container text-center">
        <h1 class="title animated" style="opacity:0;animation-delay: 0.5s;">社区集成功能应用</h1>
        <span class="small-border animated" style="opacity:0;animation-delay: 0.5s;"></span>
        <div class="row case-lists animated">
            <div class="col-md-4 col-sm-6 col-xs-6 animated" style="opacity:0;animation-delay: 1s;">
                <div class="box-item">
                    <a href="#" class="case case-box" style="background-image: url(/images/mypage/one.jpg)"></a>
                    <div class="content">
                        <p>问题帖子</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 animated" style="opacity:0;animation-delay: 1s;">
                <div class="box-item">
                    <a href="#" class="case case-box" style="background-image: url(/images/mypage/two.jpg)"></a>
                    <div class="content">
                        <p>技术文章</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 animated" style="opacity:0;animation-delay: 1s;">
                <div class="box-item">
                    <a href="#" class="case case-box" style="background-image: url(/images/mypage/three.jpg)"></a>
                    <div class="content">
                        <p>类型标签</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 animated" style="opacity:0;animation-delay: 1.5s;">
                <div class="box-item">
                    <a href="#" class="case case-box" style="background-image: url(/images/mypage/four.jpg)"></a>
                    <div class="content">
                        <p>站内通信</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 animated" style="opacity:0;animation-delay: 1.5s;">
                <div class="box-item">
                    <a href="#" class="case case-box" style="background-image: url(/images/mypage/five.jpg)"></a>
                    <div class="content">
                        <p>视频介绍</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 animated" style="opacity:0;animation-delay: 1.5s;">
                <div class="box-item">
                    <a href="#" class="case case-box" style="background-image: url(/images/mypage/six.jpg)"></a>
                    <div class="content">
                        <p>用户管理</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-six" id="section-six">
    <div class="container">
        <div class="row tech-title">
            <div class="col-md-12 text-center">
                <h1 class="animated" style="opacity:0;animation-delay: 0.5s;">技术应用</h1>
                <span class="small-border animated" style="opacity:0;animation-delay: 1s;"></span>
            </div>
        </div>
        <div class="row">
            <div class="tech-lists animated" style="opacity:0;animation-delay: 1.5s;">
                <div class="col-md-2 tech-item">
                    <a href="#">
                        <img src="/images/mypage/laravel.png" alt="">
                    </a>
                    <h4 class="skill-name">Laravel</h4>
                    <span class="h-divider"></span>
                </div>
                <div class="col-md-2 tech-item">
                    <a href="#">
                        <img src="/images/mypage/php.png" alt="">
                    </a>
                    <h4 class="skill-name">PHP</h4>
                    <span class="h-divider"></span>
                </div>
                <div class="col-md-2 tech-item">
                    <a href="#">
                        <img src="/images/mypage/vue.png" alt="">
                    </a>
                    <h4 class="skill-name">VueJs</h4>
                    <span class="h-divider"></span>
                </div>
                <div class="col-md-2 tech-item">
                    <a href="#">
                        <img src="/images/mypage/android.png" alt="">
                    </a>
                    <h4 class="skill-name">Android</h4>
                    <span class="h-divider"></span>
                </div>
                <div class="col-md-2 tech-item">
                    <a href="#">
                        <img src="/images/mypage/java.png" alt="">
                    </a>
                    <h4 class="skill-name">Java</h4>
                    <span class="h-divider"></span>
                </div>
                <div class="col-md-2 tech-item">
                    <a href="#">
                        <img src="/images/mypage/html.png" alt="">
                    </a>
                    <h4 class="skill-name">Html</h4>
                    <span class="h-divider"></span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-three" id="section-three">
    <div class="container text-center">
        <div class="video-banner">
            <div class="banner-content">
                <div class="slogan animated" style="opacity:0;animation-delay: 0.5s;">追求你自己的工作方式
                </div>
                <div class="title  animated" style="opacity:0;animation-delay: 1s;">
                    Nothing Is Impossible
                </div>
            </div>
            <div class="video-mask">
                <div class="embed-responsive embed-responsive-16by9">
                    <video class="embed-responsive-item" autoplay="autoplay"
                           poster="https://dn-st.teambition.net/site/v2.2.3/images/index/video-banner-poster.png"
                           loop="" muted="true">
                        <source src="https://dn-site.oss.aliyuncs.com/videos%2Fgo_to_work_bg_music.mp4?response-content-type=video/mp4"
                                type="video/mp4">
                    </video>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-five" id="section-five">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="animated animated-up" style="animation-delay: 0.5s;opacity: 0">心灵鸡汤</h1>
                <span class="small-border animated animated-up" style="animation-delay: 1s;opacity:0;"></span>
            </div>
        </div>
        <div class="soul-lists row">
            <div class="soul-wrapper-outer">
                <div class="soul-item animated animated-left" style="opacity:0;animation-delay: 1s;">
                    <div class="col-md-4 item">
                        <div class="de_testi">
                            <blockquote>
                                <p>真挚的友谊是人生最宝贵的财富。他取决于你的人品和性格，珍惜好你身边的每一位朋友，能留下来陪你的除了你的爱人就是你那些朋友了</p>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="soul-item animated animated-up" style="opacity:0;animation-delay: 1.5s;">
                    <div class="col-md-4 item">
                        <div class="de_testi">
                            <blockquote>
                                <p>学会放手，即使你没有太多的资本。但人生就是这样，总会有你做决定的时候，有哦时候学会放手重新开始，相信自己，没有什么办不到的</p>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="soul-item animated animated-right" style="opacity:0;animation-delay: 2s;">
                    <div class="col-md-4 item">
                        <div class="de_testi">
                            <blockquote>
                                <p>热爱生活，生活依旧是多彩的。如果让成天的工作和生活的琐事困扰自己，那么人生会一直很压抑，享受生活才能更好工作和学习，才能更好的为别人创造价值</p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <section class="section-four" id="section-four">
        <div class="container">
            <div class="row footer-nav">
                <div class="clearfix">
                    <dl class="col-sm-3 col-md-3">
                        <dt>网站相关</dt>
                        <dd><a>文章教程</a></dd>
                        <dd><a href="http://hao.shejidaren.com/" target="_blank">资源网站</a></dd>
                        <dd><a>社区规则</a></dd>
                    </dl>
                    <dl class="col-sm-3 col-md-3 site-link">
                        <dt>学习资料</dt>
                        <dd><a href="https://laravel.com/" target="_blank">Laravel官网</a></dd>
                        <dd><a href="http://laravelacademy.org/" target="_blank">Laravel学院</a></dd>
                        <dd><a href="https://laravel-china.org/" target="_blank">PHPHub</a></dd>
                        <dd><a href="https://easywechat.org/" target="_blank">EasyWechat</a></dd>
                    </dl>
                    <dl class="col-sm-3 col-md-3 site-link">
                        <dt>常用链接</dt>
                        <dd><a>域名注册</a></dd>
                        <dd><a href="https://unsplash.com/" target="_blank">图片资源</a></dd>
                        <dd><a href="http://pkg.phpcomposer.com/" target="_blank">Composer镜像</a></dd>
                    </dl>
                    <dl class="col-sm-3 col-md-3 site-link">
                        <dt>个人相关</dt>
                        <dd><a href="http://jellybook.me" target="_blank">个人博客</a></dd>
                        <dd><a href="http://weibo.com/geekghc" target="_blank">个人微博</a>
                        </dd>
                        <dd><a href="https://github.com/GeekGhc" target="_blank">Github</a></dd>
                        <dd><a href="https://segmentfault.com/u/geekghc" target="_blank">Segmentfault</a></dd>
                    </dl>
                </div>
            </div>
        </div>
    </section>
    <footer class="site-footer">
        <div id="backToTop" class="scroll-back-to-top" style="display: none">
            <button class="ui black button"><i class="chevron up icon" style="margin: 0px -0.214286em;"></i></button>
        </div>
        <div class="container">
            <div class="row">
                <div class="co-md-12 text-center">
                    <div class="copyright">
                        <p>
                            <a href="#">CodeSpace</a>
                            | Copyright © 2017 Developed by JellyBean. <i class="fa fa-coffee"></i>
                        </p>
                        <p><a style="font-weight: bold" href="http://www.miitbeian.gov.cn"
                              target="_blank">苏ICP备16045385号-2</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right"></div>
    </footer>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/source/bootstrap.min.js"></script>
<script src="/js/index.js"></script>
</body>
</html>