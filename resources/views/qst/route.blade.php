<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.bootcss.com/semantic-ui/2.2.10/semantic.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/qst/css/style.css">
    <title>QST路线</title>
</head>
<body>
<div class="header-title">
    <h1>7月活动安排方案</h1>
</div>
<div class="ui equal width grid">
    <div class="equal width row">
        <div class="column">
            <h2 id="aaa">A线路</h2>
            <div class="example-a">
                <div class="ui card red station-card">
                    <div class="content route-a">
                        <h4 class="location">线路图</h4>
                        <h4 class="location">徒步耗时</h4>
                        <h4 class="location-time">(用时预估)</h4>
                    </div>
                    <div class="extra content route-time">
                        <h4>时间预估</h4>
                    </div>
                    <div class="content supply-site">
                        <h4>补给站</h4>
                    </div>
                </div>
                <div class="extra-info">
                    <div class="item">
                        <span class="title">总距离</span>16.1公里
                    </div>
                    <div class="item">
                        <span class="div-time">总用时</span>315.0分钟 |
                        <time>5.3小时</time>
                    </div>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">出发站</div>
                </div>
                <a class="content route-a" href="{{url('qst/barrier')}}">
                    <h4 class="location">基地</h4>
                </a>
                @if($listA->get('基地'))
                    @foreach($listA->get('基地') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">8:30-8:50</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>2.1公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>25分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第一站</div>
                </div>
                <a class="content route-a" href="{{url('qst/barrier')}}">
                    <h4 class="location">龙游河公园</h4>
                    <div class="location-time">
                        15分钟
                    </div>
                </a>
                @if($listA->get('龙游河公园'))
                    @foreach($listA->get('龙游河公园') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">9:15-9:30</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>1.6公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>20分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第二站</div>
                </div>
                <a class="content route-a" href="{{url('qst/barrier')}}">
                    <h4 class="location mark">大润发</h4>
                    <div class="location-time">
                        15分钟
                    </div>
                </a>
                @if($listA->get('大润发'))
                    @foreach($listA->get('大润发') as $item)
                    <div class="content team" style=" background-color: #dff0d8;">
                        <h4>{{$item->team}}</h4>
                    </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">9:50-10:05</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>1.4公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>20分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第四站</div>
                </div>
                <a class="content route-a" href="{{url('qst/barrier')}}">
                    <h4 class="location">安定广场</h4>
                    <div class="location-time">
                        10分钟
                    </div>
                    <div class="location-time mark">
                        10分钟
                    </div>
                </a>
                @if($listA->get('安定广场'))
                    @foreach($listA->get('安定广场') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">10:25-11:45</button>
                </div>
                <div class="content supply-site">
                    <h4>补给站</h4>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>2.5公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>30分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第五站</div>
                </div>
                <a class="content route-a" href="{{url('qst/barrier')}}">
                    <h4 class="location">水绘园</h4>
                    <div class="location-time">
                        15分钟
                    </div>
                </a>
                @if($listA->get('水绘园'))
                    @foreach($listA->get('水绘园') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">12:15-12:30</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p class="mark">4.2公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>55分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第六站</div>
                </div>
                <a class="content route-a" href="{{url('qst/barrier')}}">
                    <h4 class="location">红十四军</h4>
                    <div class="location-time">
                        25分钟
                    </div>
                    <div class="location-time mark">
                        5分钟
                    </div>
                </a>
                @if($listA->get('红十四军'))
                    @foreach($listA->get('红十四军') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">13:25-13:55</button>
                </div>
                <div class="content supply-site">
                    <h4>补给站</h4>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>2.5公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>30分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第七站</div>
                </div>
                <a class="content route-a" href="{{url('qst/barrier')}}">
                    <h4 class="location">奥体中心</h4>
                    <div class="location-time">
                        15分钟
                    </div>
                </a>
                @if($listA->get('奥体中心'))
                    @foreach($listA->get('奥体中心') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">14:25-14:40</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>1.8公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>25分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">终点站</div>
                </div>
                <a class="content route-a" href="{{url('qst/barrier')}}">
                    <h4 class="location">基地</h4>
                </a>
                @if($listA->get('基地'))
                    @foreach($listA->get('基地') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">15:05-15:20</button>
                </div>
            </div>
        </div>
        <div class="column">
            <h2>B线路</h2>
            <div class="example-b">
                <div class="ui card red station-card">
                    <div class="content route-b">
                        <h4 class="location">线路图</h4>
                        <h4 class="location">徒步耗时</h4>
                        <h4 class="location-time">(用时预估)</h4>
                    </div>
                    <div class="extra content route-time">
                        <h4>时间预估</h4>
                    </div>
                    <div class="content supply-site">
                        <h4>补给站</h4>
                    </div>
                </div>
                <div class="extra-info">
                    <div class="item">
                        <span class="title">总距离</span>16.1公里
                    </div>
                    <div class="item">
                        <span class="div-time">总用时</span>315.0分钟 |
                        <time>5.3小时</time>
                    </div>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">出发站</div>
                </div>
                <a class="content route-b" href="{{url('qst/barrier')}}">
                    <h4 class="location">基地</h4>
                </a>
                @if($listB->get('基地'))
                    @foreach($listB->get('基地') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">8:30-8:50</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>1.8公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>20分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第一站</div>
                </div>
                <a class="content route-b" href="{{url('qst/barrier')}}">
                    <h4 class="location">奥体中心</h4>
                    <div class="location-time">
                        15分钟
                    </div>
                </a>
                @if($listB->get('奥体中心'))
                    @foreach($listB->get('奥体中心') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">9:10-9:25</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>2.5公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>30分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第二站</div>
                </div>
                <a class="content route-b" href="{{url('qst/barrier')}}">
                    <h4 class="location">红十四军</h4>
                    <div class="location-time">
                        20分钟
                    </div>
                    <div class="location-time mark">
                        15分钟
                    </div>
                </a>
                @if($listB->get('红十四军'))
                    @foreach($listB->get('红十四军') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">9:35-10:00</button>
                </div>
                <div class="content supply-site">
                    <h4>补给站</h4>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p class="mark">4.2公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>55分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第三站</div>
                </div>
                <a class="content route-b" href="{{url('qst/barrier')}}">
                    <h4 class="location">水绘园</h4>
                    <div class="location-time">
                        15分钟
                    </div>
                </a>
                @if($listB->get('水绘园'))
                    @foreach($listB->get('水绘园') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">10:55-11:10</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>2.5公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>30分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第四站</div>
                </div>
                <a class="content route-b" href="{{url('qst/barrier')}}">
                    <h4 class="location">安定广场</h4>
                    <div class="location-time">
                        20分钟
                    </div>
                    <div class="location-time mark">
                        10分钟
                    </div>
                </a>
                @if($listB->get('安定广场'))
                    @foreach($listB->get('安定广场') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">11:40-12:10</button>
                </div>
                <div class="content supply-site">
                    <h4>补给站</h4>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>1.4公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>25分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第六站</div>
                </div>
                <a class="content route-b">
                    <h4 class="location mark" href="{{url('qst/barrier')}}">大润发</h4>
                    <div class="location-time">
                        15分钟
                    </div>
                </a>
                @if($listB->get('大润发'))
                    @foreach($listB->get('大润发') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">12:25-12:50</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>1.6公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>15分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">第七站</div>
                </div>
                <a class="content route-b" href="{{url('qst/barrier')}}">
                    <h4 class="location">龙游河公园</h4>
                    <div class="location-time">
                        15分钟
                    </div>
                </a>
                @if($listB->get('龙游河公园'))
                    @foreach($listB->get('龙游河公园') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">13:05-14:20</button>
                </div>
            </div>
            <div class="middle-div">
                <div class="div-time">
                    <p>2.1公里</p>
                </div>
                <img src="./img/down.png" alt="">
                <div class="div-distance">
                    <p>25分钟</p>
                </div>
            </div>
            <div class="ui card red station-card">
                <div class="content route-name">
                    <div class="header">终点站</div>
                </div>
                <a class="content route-b" href="{{url('qst/barrier')}}">
                    <h4 class="location">基地</h4>
                </a>
                @if($listB->get('基地'))
                    @foreach($listB->get('基地') as $item)
                        <div class="content team" style=" background-color: #dff0d8;">
                            <h4>{{$item->team}}</h4>
                        </div>
                    @endforeach
                @endif
                <div class="extra content route-time">
                    <button class="ui button">14:45-15:00</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>