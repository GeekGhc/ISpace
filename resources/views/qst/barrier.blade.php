<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>关卡说明</title>
    <link href="https://cdn.bootcss.com/semantic-ui/2.2.10/semantic.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/qst/css/style.css">
</head>
<body>
<div class="container">
    <div class="ui three column centered grid">
        <div class="column">
            <table class="ui celled striped yellow table">
                <thead class="table-head">
                <tr><th colspan="3">关卡说明</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="collapsing">任务说明:</td>
                    <td>挑战不可能</td>
                </tr>
                <tr>
                    <td>任务规则:</td>
                    <td>裁判老师说出一句话(不少于8个字)每个参赛人员拿到句子后,将内容倒过来说,没完成一题可以获得相应的经验,计时两分钟</td>
                </tr>
                <tr>
                    <td>任务要求:</td>
                    <td>其他队员不得提醒</td>
                </tr>
                <tr>
                    <td>获得经验:</td>
                    <td>10分</td>
                </tr>
                <tr>
                    <td class="参赛人数">任务要求:</td>
                    <td>5人</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
