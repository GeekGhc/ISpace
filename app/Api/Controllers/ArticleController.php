<?php
namespace App\Api\Controllers;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    //获取热门文章
    public function getHotList(Request $request)
    {
        $limit = $request->get('limit')?$request->get('limit'):5;
        $articles = Article::with('user')->orderBy('comment_count','DESC')->limit($limit)->get();
        $colection = $articles->map(function($item,$key){
            $item->createdAt = $item->created_at->diffForHumans();
            $item->commentCount = $item->comment_count;
            $item->author = $item->user->name;
        });
        if($articles){
            return response()->json([
                'code'=>1,
                'message'=>'查询成功',
                'data'=>$articles
            ]);
        }
        return response()->json([
            'code'=>0,
            'message'=>'没有数据',
            'data'=>null
        ]);
    }

}