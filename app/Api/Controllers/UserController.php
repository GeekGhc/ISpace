<?php

namespace App\Api\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{

    //获取当前用户
    public function getUser($userId)
    {
        $user = User::find($userId);
        $user->createdAt = $user->created_at->diffForHumans();
        $user->followersCount = $user->followers_count;
        $user->followingsCount = $user->followings_count;
        $user->site = $user->profile->website;
        $user->description = $user->profile->description;
        if ($user) {
            return response()->json([
                'code' => 1,
                'data' => $user,
                'message' => '查找成功'
            ]);
        }
        return response()->json([
            'code' => 0,
            'data' => null,
            'message' => '没有此用户'
        ]);
    }

    //用户登录
    public function login(Request $request)
    {
        \Log::info($request->all());
        if (Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'is_confirmed' => 1,
        ])
        ) {
            $user = User::where('email', $request->get('email'))->first();
            return response()->json([
                'code' => 1,
                'data' => $user,
                'message' => 'success'
            ]);
        };
        return response()->json([
            'code' => 0,
            'data' => null,
            'message' => '登录失败'
        ]);
    }

    //用户注册
    public function register(Request $request)
    {
        $data = [
            'avatar' => '/images/avatar/default.png',
            'confirm_code' => str_random(48),
            'user_name' => $request->get('name'),
            'api_token' => str_random(60),
        ];
        $res = User::register($request->all(), $data);
//        $res = User::find(11);
        if (!$res) {
            return response()->json([
                'code' => 0,
                'data' => null,
                'message' => 'failed'
            ]);
        }
        $user = User::with('profile')->find($res->id);
        return response()->json([
            'code' => 1,
            'data' => $user,
            'message' => 'success'
        ]);
    }

    //密码修改
    public function editPwd(Request $request)
    {
        $userId = $request->get('userId');
        $user = User::find($userId);
        if (\Hash::check($request->get('old_password'), $user->password)) {
            $user->password = $request->get('password');
            if ($user->save()) {
                return response()->json([
                    'code' => 1,
                    'data' => $user,
                    'message' => 'success'
                ]);
            }
            return response()->json([
                'code' => 0,
                'data' => null,
                'message' => '用户密码修改失败'
            ]);
        }
        return response()->json([
            'code' => 0,
            'data' => null,
            'message' => '原密码错误'
        ]);

    }
}